# 微信小程序星级评分组件

#### 介绍
适用于微信小程序的星级评分组件。

<img src="assets/image-20190726142224254.png" width="30%" />

<img src="assets/image-20190726142314582.png" width="30%" />

#### 使用说明

1. 组件目录

   <img src="assets/image-20190726143443171.png" width="30%" />

2. 在要添加的页面的json文件中加上下面语句，例如index.json

   ```json
   {
     "usingComponents": {
       "rateStar": "/components/rateStar/rateStar"
     }
   }
   ```

3. 在index.wxml中使用组件

   ```html
   <!--index.wxml-->
   <view class="container">
     <view>评分1</view>
     <view class="stars">
       <rateStar class="stars" rateObject="0" bind:change="getScore" >
       </rateStar>
     </view>
     <view>评分2</view>
     <view class="stars">
       <rateStar class="stars" rateObject="1" bind:change="getScore" >
       </rateStar>
     </view>
   </view>
   ```